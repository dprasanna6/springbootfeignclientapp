package com.feignclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.feignclient.ribbonconfig.RibbonConfiguration;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@RibbonClient(name="feignOrderClient", configuration = RibbonConfiguration.class)
public class SpringbootFeignclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFeignclientApplication.class, args);
	}

}
