package com.feignclient.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.feignclient.FeignOrderClient;
import com.feignclient.dto.Order;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	FeignOrderClient feignOrderClient;
    
	@GetMapping("/info")
	public String getInfo() {
		return feignOrderClient.getInfo();
	}
	
	@GetMapping("")
	public List<Order> getUserOrders() {

		return feignOrderClient.getAll();

	}
	
	@GetMapping("/{userId}")
	public List<Order> getUserOrdersById(@PathVariable String userId) {
		return feignOrderClient.getAllById(userId);
	}
	
	@GetMapping("/byparam")
	public List<Order> getUserOrdersByReqParam(@RequestParam String userId) {
		return feignOrderClient.getAllByReqParam("112233");
	}
	
	@GetMapping("/postparam")
	public List<Order> testPostWithParam(@RequestParam String userId){
		return feignOrderClient.getAllByPostReqParam(userId);
	}
	
	@GetMapping("/bybody")
	public Order testPostWithBody(){
		Order order = new Order();
		order.setId(1122);
		order.setDes("Feign user");
		return feignOrderClient.getAllByPostReqBody(order);
	}
     
	 

}
